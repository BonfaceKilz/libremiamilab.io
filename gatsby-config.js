module.exports = {
    siteMetadata: {
      title: `LibreMiami | Miami Libre Free & Open Source Advocacy Group`,
      description: `Libre free & open source software advocacy group in Miami & Fort Lauderdale. Find software. Learn to code. Contribute to open source. Join today!`,
      author: `LibreMiami`,
      siteUrl: `https://libremiami.org`,
    },
    plugins: [
    {
      resolve: `gatsby-plugin-postcss`,
      options: {
        cssLoaderOptions: {
          camelCase: false,
        },
      },
    },
    {
      resolve: "gatsby-plugin-seo",
      options: {
        siteName: "LibreMiami",
        defaultSiteImage: "/images/LibreMiamiSocial.png",
        siteUrl: "https://libremiami.org",
      }
    },
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/content/blog`,
        name: `blog`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `data`,
        path: `${__dirname}/src/data/`,
        ignore: [`**/\.*`], // ignore files starting with a dot
      },
    },
    {
      resolve: `gatsby-transformer-remark`,
      options: {
        plugins: [
          {
            resolve: `gatsby-remark-images`,
          },
          {
            resolve: `gatsby-remark-responsive-iframe`,
            options: {
              wrapperStyle: `margin-bottom: 1.0725rem`,
            },
          },
          `gatsby-remark-prismjs`,
          `gatsby-remark-copy-linked-files`,
          `gatsby-remark-smartypants`,
        ],
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `LibreMiami`,
        short_name: `LibreMiami`,
        icon: `src/images/favicon.png`,
        start_url: `/`,
        display: `browser`,
        background_color: `#1D3034`,
        theme_color: `#ffffff`,
      }
    },
    `gatsby-plugin-sitemap`,
    `gatsby-transformer-json`,
  ]
}
