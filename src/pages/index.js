import React from 'react';
import './index.css';
import App from '../components/App';
import {Helmet} from "react-helmet";

export default () => {
    return (
        <>
            <Helmet>
                <title>LibreMiami | Miami Libre Free & Open Source Advocacy Group</title>
                <meta name="description" content="Libre free & open source software advocacy group in Miami & Fort Lauderdale. Find software. Learn to code. Contribute to open source. Join today!" />
                <meta property="og:url" content="https://libremiami.org/" />
                <meta property="og:type" content="website" />
                <meta property="og:title" content="LibreMiami | Miami Libre Free & Open Source Advocacy Group" />
                <meta property="og:description" content="Libre free & open source software advocacy group in Miami & Fort Lauderdale. Find software. Learn to code. Contribute to open source. Join today!" />
                <meta property="og:locale" content="en_US" />
                <meta property="og:site_name" content="LibreMiami" />
                <meta property="og:image" content="https://libremiami.org/images/LibreMiamiSocial.png" />
                <meta property="og:image:secure_url" content="https://libremiami.org/images/LibreMiamiSocial.png" />
                <meta name="twitter:card" content="summary_large_image" />

            </Helmet>
            <App />
        </>
    )
} 

