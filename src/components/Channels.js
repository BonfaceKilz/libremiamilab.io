import React from 'react'

export default function Channels() {
    return (
        <section id="channels" class="container">
            <div>
                <h2>Say Hi!</h2>
                    <p>Currently, you can reach out to the community either through Matrix or on IRC; we've created a bridge between them.</p>

                        <small><i>PS: Is your favourite chat platform missing out? Ping us in one of our channels listed below with a request to add said platform!</i></small> <br/>
                        <h4>Guix Packaging:</h4>
                    <p>This channel contains conversations about packaging software within Guix or channels of interest. Conversations around extending guix are also welcome!</p>
                    <ul>
                        <li><a href="https://riot.im/app/#/room/#guix-packaging:matrix.org" target="_blank">Join the chat</a> in Matrix.</li>
                        <li>Join us on the <code>#libremiami-guixpackagers</code> channel on the <a href="https://en.wikipedia.org/wiki/Freenode" target="_blank">Freenode IRC network</a>.</li>
                    </ul>
                        <h4>General Topics:</h4>
                    <p>This channel contains conversations about Free(As in Freedom) software and any other hot topics that are relevant to LibreMiami's mission</p>
                    <ul>
                        <li><a href="https://riot.im/app/#/room/!bjpDRCNuFsHwtGwgsA:matrix.org?via=matrix.org" target="_blank">Join the chat</a> in Matrix.</li>
                        <li>Join us on the <code>#libremiami</code> channel on the <a href="https://en.wikipedia.org/wiki/Freenode" target="_blank">Freenode IRC network</a>.</li>
                        </ul>
                    <h4>Voice Chat:</h4>
                    <p>Wanna hangout with someone over a voice channel? Head over to our mumble instance <a href="http://mumble.libremiami.org/" target="_blank">here</a>!</p>

                    <h4>Kanban Board:</h4>
                    <p>You can have an  overview of some of the packages we are working on in our Taiga instance <a href="https://board.disroot.org/project/guix-packaging/kanban" target="_blank">here</a></p>
            </div>
        </section>
    )
}
