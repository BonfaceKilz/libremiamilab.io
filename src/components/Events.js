import React from 'react'
import "./Events.css"

export default function Events() {
    return (
        <section id="events" class="events-section container">
            <div id="meetups">
                <h2>Regular Meetups</h2>
                <p>To keep everyone safe from COVID-19, we&rsquo;re doing online meetups the first Saturday of every month, 4-6pm. Check <a href="https://gettogether.community/libreplanet-south-florida/">gettogether</a> or <a href="https://riot.im/app/#/room/!bjpDRCNuFsHwtGwgsA:matrix.org?via=matrix.org">the chat</a> for the latest.</p>
                

            </div>
        </section>
    )
}
