import React from 'react'
import "./NavBar.css"

export default function NavBar() {
    return (
        <header class="nav-bar">
            <img class="logo" alt=""/>
            <nav>
                <a href="/#home">Home</a>
                <a href="/#about">About</a>
                <a href="/#events">Events</a>
                <a href="/#blog">Blog</a>
            </nav>
        </header>
    )
}
