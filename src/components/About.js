import React from 'react'
import './About.css'
import Profile from "./Profile"
import { Link, graphql, useStaticQuery } from "gatsby"

export default function About() {
    const data = useStaticQuery(graphql`
        query {
            members: allMembersJson {
                nodes {
                    title
                    handle
                    description
                    picture
                    website
                }
            }
        }
    `);
    const members = data.members.nodes;
    return (
        <section id="about" class="about-section container">
            <div class="about-text-container">
                <div>
                    <h2>Our Mission</h2>
                    <p>We believe you should be free to use your software as you wish, adapt it to your needs (maybe with some help), and disseminate it through your community. We promote libre software like LibreOffice, Firefox and GNU/Linux which respects your freedom over proprietary software like Microsoft Office, Edge, and Windows which restricts users.</p>
                    <p>LibreMiami is here to build a libre software utopia, by sharing knowledge with users and developers, advocating for user freedom, and advancing the state of libre software. </p>
                    <h3>Okay, but what do you do specifically?</h3>
                    <p>We help people meet their software and hardware needs. We help people learn to code. We help people contribute to open source. We fearlessly tackle the big issues. We work on what we really care about. We hang out in pink rooms, drink boba tea and laugh at our commit messages.</p>
                </div>
                <div id="join">
                    <h2>Join Us!</h2>
                    <p>Get involved in 3 easy steps:</p>
                    <ol>
                        <li>Make a matrix account through <a href="https://app.element.io/#/register">Element.io</a>. Then <a href="https://riot.im/app/#/room/!bjpDRCNuFsHwtGwgsA:matrix.org?via=matrix.org">join the chat</a> to stay in touch.</li>
                        <li>If you want to code, make sure you have a <a href="https://gitlab.com/users/sign_in#register-pane">GitLab</a> account. We stay (dis)organized in <a href="https://gitlab.com/libremiami">our GitLab group</a></li>
                        <li>Come to <a href="#events">a meetup near you</a>. You won&rsquo;t regret it!</li>
                    </ol>
                    <p>You might also want to check out <a href="https://gitlab.com/libremiami/libremiami/wikis/Newbie-Guide">the newbie guide</a>.</p>
                </div>
            </div>
            <h2 style={{marginTop: `1em`, textAlign: `center`}}>Contributors</h2>
            <div className="profiles">
                { members.map(member => <Profile title={member.title} handle={member.handle} description={member.description} picture={member.picture} website={member.website} />) }
            </div>
        </section>
    )
}
