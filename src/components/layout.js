import React from "react"
import NavBar from "../components/NavBar"


const Layout = ({ location, title, children }) => {
  return (
    <>
      <NavBar />
      <main>{children}</main>
    </>
  )
}

export default Layout
