import React from 'react'
import style from "./Profile.module.css"

export default function Profile(props) {
    // dynamically referencing images is an acceptable use of the static folder per docs
    const picturePath = "/images/members/" + props.picture
    return (
        <div className={style.profile}>
            <a href={props.website}>
                <img src={picturePath}/>
                <p className={style.handle}>{props.handle}</p>
                <p className={style.title}>{props.title}</p>
            </a>
        </div>
    )
}