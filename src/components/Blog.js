import React from "react"
import { Link, graphql, StaticQuery } from "gatsby"
import "./Blog.css";
import Img from "gatsby-image"

export default Blog => (
  <StaticQuery
  query = { graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
    allMarkdownRemark(sort: { fields: [frontmatter___date], order: DESC }) {
      edges {
        node {
          excerpt
          fields {
            slug
          }
          frontmatter {
            date(formatString: "MMMM DD, YYYY")
            title
            description
            featuredImageCredit
            featuredImageLicense
            featuredImageLicenseLink
            featuredImage {
              childImageSharp {
                fluid {
                  ...GatsbyImageSharpFluid
                }
              }
            }
          }
        }
      }
    }
  }
`

  }
  render = {data => (
    <section id="blog" class="container blog-section">
      <h2>Recent Posts</h2>
      <div class="post-abstracts">
      {data.allMarkdownRemark.edges.map(({ node }) => {
          const title = node.frontmatter.title || node.fields.slug
          return (
              <article class="post-abstract" key={node.fields.slug}>
                <header>
                  <Link style={{ boxShadow: `none` }} to={node.fields.slug}>
                    <Img fluid={node.frontmatter.featuredImage.childImageSharp.fluid} />
                  </Link>
                  <h3>
                    <Link style={{ boxShadow: `none` }} to={node.fields.slug}>
                      {title}
                    </Link>
                  </h3>
                  <small>{node.frontmatter.date}</small>
                </header>
                <section>
                  <p class="post-abstract-description"
                    dangerouslySetInnerHTML={{
                      __html: node.frontmatter.description || node.excerpt,
                    }}
                  />
                </section>
                <footer>
                  {node.frontmatter.featuredImageCredit && <><small>Image by {node.frontmatter.featuredImageCredit}. License: <a href={node.frontmatter.featuredImageLicenseLink}>{node.frontmatter.featuredImageLicense}</a></small></>}
                </footer>
              </article>
          )
        })
      }
      </div>
    </section>
  )}
  />
)
