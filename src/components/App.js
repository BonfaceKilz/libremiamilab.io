import React from 'react';
import NavBar from './NavBar';
import Hero from './Hero';
import About from './About';
import Events from './Events';
import Blog from './Blog';
import Channels from './Channels';

function App() {
  return (
    <>
      <NavBar />
      <main>
        <Hero />
        <About />
        <Channels />
        <Events />
        <Blog />
      </main>
    </>
  );
}

export default App;
