import React from 'react'
import './Hero.css'
import ButtonLink from './ButtonLink'

export default function Hero() {
    return (
        <section id="home" class="hero-section container">
            <h1>Libre<span class="heavy">Miami</span></h1>
            <p class="hero-subheading">Advocating libre and open source software for Miami (and beyond)</p>
            <div class="hero-buttons">
                <ButtonLink link="#join" text=">_Join" />
                <ButtonLink link="https://gettogether.community/events/6369/libremiami-virtual-meetup/" text="Next Meetup" />
            </div>
        </section>
    )
}
