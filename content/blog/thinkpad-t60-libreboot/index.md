---
title: Librebooting a Thinkpad T60
date: "2020-07-20"
description: "You can install Libreboot firmware on a Thinkpad T60 without any special equipment. It’s easy if you know what steps to follow in the docs. We break it down for you in this helpful blog and video, so check it out!"
featuredImage: "./t60.jpg"
featuredImageCredit: "Kreuzschnabel"
featuredImageLicense: "CC BY-SA 3.0"
featuredImageLicenseLink: "https://creativecommons.org/licenses/by-sa/3.0/deed.en"
---

The Thinkpad T60 is one of a handful of computers supported by Libreboot. With Libreboot installed, your computer will use libre software as soon as it boots up instead of a proprietary BIOS. This is different from installing a libre OS (like GNU/Linux) to replace Windows or macOS, which is a lot easier (just get in touch if you need some help with that). While it's always best to use the official docs, the information can sometimes be a bit scattered and terse. We thought it would be nice to put together a step-by-step guide to help give a clearer overview of the process.

This information is presented in good faith, but without any warranty. Continue at your own risk. With stuff like this, there's always some risk of damaging or bricking your computer.

## Why use and Libreboot a T60?

So you might be wondering what's so great about using a computer from 2005 and if it's even worth it. Truth is, with the right GNU/Linux system, you need surprisingly little computing power to accomplish day-to-day tasks. You can even throw a bunch of relatively "bloated" apps at it and it will chug along just fine. 

Of course, you'll be pretty limited in the graphics department, but there's an oldschool charm to the 4:3 aspect ratio that's on most of these. Just watch something like *Samurai Jack* and you'll see what I mean. Some people find 4:3 more pleasant to code on too.

Old Thinkpads are also built like **tanks**, and if you do manage to break something (or just want to make some mods) they are *really* easy to work on and parts are plentiful.

This is also likely the cheapest way to go full libre. In fact, the prices bottomed out a few years ago and will likely go up as the T60 becomes a "retro" computer. At the time of writing though, you can still definitely be all-in for less than $100, probably closer to $50.

Some of the things libreboot brings to the table (other than being libre, of course):
* Faster boot
* Customizable boot screen
* Ability to encrypt /boot
* Removal of the WiFi card whitelist, so you can use a libre-compatible WiFi card

## How to clean a used Thinkpad

More likely than not, your T60 is going to be looking a bit rough. Here are some tips to spruce it up a little.

You can clean most of the computer (and most computers for that matter) with rubbing alcohol, which can be diluted up to 50% with distilled water. The less you dilute, the quicker the alcohol will evaporate and the safer it will be to use on more sensitive parts. It’s safest to damp a cloth (preferably microfiber) or a q-tip instead of applying liquids directly on the machine.

Be extra careful not to scratch your screen by only using a very fine microfiber cloth, like the type people use to clean their glasses.

To peel off stickers, first get as much of it off with your fingers as you can. Then you can wet the remaining paper with the rubbing alcohol and scratch it off. You can get sticky residue off with goo gone. 

If there are any organic stains, like food, these come off easiest with distilled white vinegar. 

You can get rid of fine scratches and smooth out other imperfections on the cover by wet polishing with melamine foam. 

## How to prepare for a T60 Libreboot

There are a few things to make sure you have in order before you attempt to flash libreboot.

You don’t need a laptop battery, but you should definitely make sure the charger/powersupply works. It should be a 65w, 20v power supply with a barrel connector. The same charger gets used with many other Thinkpads of the era.

More likely than not, the CMOS/RTC battery is dead. You’ll know if this is the case immediately when you try to boot up the computer for the first time. The laptop will beep and complain about a CMOS error, and you’ll get dumped into the proprietary BIOS so you can set the time. If this is the case, **you must replace the dead CMOS/RTC battery before librebooting or you might brick your computer**. Follow the intructions in the service manual.

You might want to update the proprietary EC firmware to the latest version (1.07) for better battery handling since Libreboot doesn't and can't replace this. You can check the version with this command:

```
grep 'at EC' /proc/asound/cards
```

Note that the BIOS and EC firmware have different version numbers.

If you need to update, you can do it from Windows with the proprietary utility provided by Lenovo. Lenovo also provides an ISO you can burn to a CD to boot from to perform the update.

Although not strictly necessary, you might want to test the RAM and check for bad blocks on the hard drive before installing your GNU/Linux of choice.

### How to make sure your T60 is Libreboot compatible

If your T60 has ATI graphics, it won’t work as expected with Libreboot. You could maybe use it as a headless computer, via SSH and the like, but it won’t work as a regular laptop. This is because the ATI graphics are proprietary. You can check to see if you have ATI graphics by either physically inspecting the computer (there may be a sticker on the palmrest or you can look at the actual board), or by using a command like this one:

```bash
sudo lspci -v | grep VGA
```

You also want to make sure to have a [compatible panel](https://libreboot.org/docs/hardware/#supported-t60-list), or that you’re willing to risk trying an untested one.

## How to flash Libreboot on a Thinkpad T60

[Get the files you need](https://libreboot.org/download.html) and [follow the docs](https://libreboot.org/docs/install/#flashrom_lenovobios).

## What to do after installing Libreboot on a Thinkpad T60

As mentioned before, installing Libreboot allows you to replace the proprietary wifi chip with a libre-compatible one. You can get a mini PCI express card using the Atheros AR9285 chipset (e.g. Atheros AR5B95) - 802.11n. For the T60 you'll also need a half-height to full-height adaptor.

The T60 has two RAM slots, but the chipset limits RAM to 3GB.

## Thinkpad T60 Libreboot demonstration video

The process can feel a bit complicated and involved if you've never done or seen it before, so we've put together a helpful demonstration video. You can [watch it on LBRY](https://lbry.tv/@LibreMiami:7/libremiami-t60-libreboot).

