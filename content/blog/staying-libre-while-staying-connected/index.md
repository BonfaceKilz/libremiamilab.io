---
title: Staying Libre While Staying Connected
date: "2020-05-18"
description: "Since we haven't been able to have our in-person meetups anymore, LibreMiami has been looking for ways to stay connected during the current crisis. Here are our top picks so far."
featuredImage: "./matrix.png"
photoCredit: "public domain"
---

Since we haven't been able to have our in-person meetups anymore, LibreMiami has been looking for ways to stay connected during the current crisis. Here are our top picks so far.

## Jitsi Meet

Since Zoom and Microsoft Teams are both proprietary, we've been using [Jitsi Meet](https://meet.jit.si/) to have our meetups online. It's easy to use. You just go to the website and press "go" for a new room. You can share a link to the room for people to join, and once everyone leaves it just gets destroyed. It has voice, video, screen share, a chat, and a hand-raising feature. They also have mobile apps. If you're technical, you can even set up your own Jitsi server and configure it however you want. 

## Matrix
[Matrix](https://matrix.org/) is a bit harder to wrap your head around, but it's very cool. We mainly use it to chat and share files, but it also has voice and video calling. Not only is it fully libre, but it's also decentralized, like email. So anyone can set up a matrix server and people with accounts on different servers can talk to each other. Also you can use any client you want to connect. The most user-friendly client right now is [Riot.im](https://about.riot.im/) but there are dozens of clients to choose from. There are also bots and widgets, including one for integrating Jitsi so you can use both at the same time.

## Some Final Thoughts

We don't think that our choices will work for everyone, but our communities deserve better than one-size-fits-all proprietary software with potential vendor lock-in, malware, and loss of support. Let's work together to find and create better solutions!
