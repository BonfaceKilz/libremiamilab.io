---
title: Setting up node and npm on libre systems
date: "2020-04-30"
description: "Install node and npm on libre systems like Debian and Trisquel"
featuredImage: "./npm.png"
---

So you want to do some serious JavaScripting. Say no more.

## Debian and Trisquel

The version of node on Debian and Trisquel tends to be a bit dated, so you'll want to
use nvm. This gives you the added benefit of being able to switch between different node versions
and not having to `sudo` to install packages globally. [Install nvm](https://github.com/nvm-sh/nvm) (using wget is probably easiest).

Then close and reopen your terminal so it picks up the nvm command. You 
can make sure nvm is installed properly by running 

```bash
command -v nvm
```

which should output `nvm`. Use this command to install the latest long-term 
support version of npm and node:

```bash
nvm install --lts
```

You can check out the versions that got installed.

```bash
node --version
npm --version
```

That's it!
