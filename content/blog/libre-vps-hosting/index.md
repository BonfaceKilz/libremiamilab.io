---
title: Libre VPS Hosting
date: "2020-05-09"
description: "Find Libre VPS (virtual private server) hosting for websites, software applications and more."
featuredImage: "./server.jpg"
imageCredit: "Taylor Vick, Unsplash license"
---

To host our projects and instances of services we need, we went looking for virtual private server (VPS) providers that
1) Do not require proprietary JavaScript (for creating accounts, making payments, configuring the server, etc.)
2) Have images for Debian at least, if not also FSF-endorsed distros.

Ideally, we would have our own servers and people to secure and maintain them, but that's not really an option right now. There's nothing intrinsically wrong with services, and we don't think this should be something any group of people spins their wheels on. So here are the most libre options we could find. We'd like to add more to this list, so if you run a VPS service that meets the criteria, be sure to get in touch! 

## Capsul

[Capsul](https://capsul.org/) is a service run by [Cyberia Computer Club](https://cyberia.club/) volunteers in Minnesota. They take crypto. 

## Super Dimension Fortress

[Super Dimension Fortress](https://sdf.org/) or SDF is a public access Unix system. They offer a wide range of services, many for free, but running a VPS on SDF requires a sustaining membership (their highest tier). Avoiding proprietary JS requires mailing in your membership dues.

Notable Distros
* Debian

