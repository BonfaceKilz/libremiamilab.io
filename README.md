# libremiami.org

This is the git repo for the LibreMiami website. Anything that you push to the master branch gets automatically deployed to GitLab Pages according to .gitlab-ci.yml.

It uses [Gatsby](https://www.gatsbyjs.org/).

The website has a number of functions:

* Inform the public about the group
* Inform potential and current members about current happenings.
* Spotlight especially noteworthy group members
* Have informative and interesting blog posts about libre topics, especially ones that are relevant to Miami.
* Facilitate face-to-face marketing
* Market the group online by being easily found on search engines through relevant key words

Domain names are registered and configured through [nearlyfreespeech.net](https://www.nearlyfreespeech.net/) where one can purchase many web services without proprietary JavaScript.

## Contributing Blog Posts

If you're a developer, you might follow the contributing code instructions below, make a new folder with your desired slug under "content/blog" and look at other blog post folders for reference. You can create an issue with your blog idea and a new branch.

Otherwise, if you are comfortable with [markdown](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet) it helps if you format your post with it. Either way, send your draft to the chatroom or bring it to a meetup for review. 

## Contributing Code

### Setting up your local environment

This is how you set up your system to work on this project. If you run into any trouble with this, you can create an issue, jump in the chat, or come to a meetup for help or to report a problem.

Make sure you have node and npm installed, and that you have a BASH terminal or similar to run commands, and your favorite libre text editor. For now, you can just go to a meetup for help with this or using the command line if you need it. Make sure there isn't anything in your text editor that would automagically create or alter files in an unwanted way.

#### Clone the repo

Create your local copy of the project by running

```bash
git clone https://gitlab.com/libremiami/libremiami.gitlab.io.git
```
#### Install gatsby

Install the gatsby command line interface by running

```bash
npm install --global gatsby-cli
```

#### Install dependencies

Install all the programs you need to work on and build the project by being inside the project folder running

```bash
npm install
```

### Developing

If you work on an improvement, create an issue if there isn't one already for your change and work in a new branch named with the issue number and a short description of what you are doing. "42-add-meaning-of-life" for example.

Let us know in the chat when you are done.

#### Previewing changes

Use this command to serve the website to yourself locally.

```bash
gatsby develop
```

Make sure your changes build too before submitting them.

```bash
gatsby build
gatsby serve
```

## License

The code for the website is distributed under GPLv3. See LICENSE for details. Unless otherwise noted, the text of blogs may be used under the terms of the Creative Commons Attribution-ShareAlike 4.0 International license. Images are copyright their respective owners and are used with permission or under a suitable license. Please see code or contact us for details.

